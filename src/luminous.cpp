#include "Math/Vec.h"
#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <stdio.h>
#include "Utility/Logger.h"

using namespace std;

void display(void)
{
	GLfloat vertices[] = {
			-0.7f, 0.7f, 0.0f,
			0.7f, 0.7f, 0.0f,
			0.7f, -0.7f, 0.0f,
			-0.7f, -0.7f, 0.0f
	};

	GLuint vaoID, vboID;
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);

	glGenBuffers(1, &vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, 12*sizeof(GLfloat), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_QUADS, 0, 4);

	glutSwapBuffers();
}

int main(int argc, char **argv)
{
	FILE *f;
	f = fopen("")
	Logger::getInstance()->setOutputFile(stderr);
	INFO("lo%dl", 2);
	glutInit(&argc, argv);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(50, 50);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutCreateWindow("Test");
	glewInit();

	GLenum prog;

	FILE *fp;
	fp = fopen("./Shader/vs.glsl", "r");
	fseek(fp, 0, SEEK_END);
	GLint size = ftell(fp);
	rewind(fp);
	char buffer[size];
	fread(buffer, size, 1, fp);
	fclose(fp);
	GLenum vs = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
	glShaderSourceARB(vs, 1, (const char**)&buffer, NULL);
	glCompileShaderARB(vs);
	glAttachObjectARB(prog, vs);

	fp = fopen("./Shader/fs.glsl", "r");
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	rewind(fp);
	fread(buffer, size, 1, fp);

	GLenum fs = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
	glShaderSourceARB(fs, 1, (const char**)&buffer, NULL);
	glCompileShaderARB(fs);
	glAttachObjectARB(prog, fs);

	glLinkProgramARB(prog);
	glUseProgramObjectARB(prog);

	glutDisplayFunc(display);
	glutMainLoop();

	return 0;
}
