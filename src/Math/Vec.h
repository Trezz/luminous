/*
 * Vec.h
 *
 *  Created on: 04.08.2014
 *      Author: Flo
 */

#ifndef VEC_H_
#define VEC_H_

#include <math.h>

/** Base vector class to provide functions and operators for specialized subclasses.
 * A abstract vector class, which provides all basic operators and functions. Specialized
 * vector classes have to extend this class.
 * @tparam T The type of the vector coordinates (float, int, ...).
 * @tparam size The size of the vector.
 */
template<typename T, int size>
class VecBase_A
{
public:
	virtual ~VecBase_A()
	{
	};
	/** Calculates the length of the vector.
	 * Calculates the length / norm / magnitude of the vector.
	 * @return The length of the vector.
	 */
	virtual float lenght()
	{
		float length = 0.0f;
		for(int i=0; i<size; i++)
		{
			length += this->_data[i] * this->_data[i];
		}

		return sqrt(length);
	}
	virtual T operator[](const int index)
	{
		return this->_data[index];
	}
	virtual bool operator==(const VecBase_A &other)
	{
		bool isEqual = true;
		for(int i=0; i<size; i++)
		{
			isEqual &= this->_data[i] == other._data[i];
		}

		return isEqual;
	}
	virtual bool operator!=(const VecBase_A &other)
	{
		return !(*this == other);
	}
	virtual VecBase_A& operator=(const VecBase_A &other)
	{
		for(int i=0; i<size; i++)
		{
			this->_data[i] = other._data[i];
		}

		return *this;
	}
	virtual VecBase_A& operator+=(const VecBase_A &other)
	{
		for(int i=0; i<size; i++)
		{
			this->_data[i] += other._data[i];
		}

		return *this;
	}
	virtual VecBase_A& operator-=(const VecBase_A &other)
	{
		for(int i=0; i<size; i++)
		{
			this->_data[i] -= other._data[i];
		}

		return *this;
	}
	virtual VecBase_A& operator*=(const float scalar)
	{
		for(int i=0; i<size; i++)
		{
			this->_data[i] *= scalar;
		}

		return *this;
	}
	virtual VecBase_A& operator/=(const float scalar)
	{
		for(int i=0; i<size; i++)
		{
			this->_data[i] /= scalar;
		}

		return *this;
	}

	virtual VecBase_A operator+(const VecBase_A &other)
	{
		VecBase_A temp(*this);
		temp += other;

		return temp;
	}
	virtual VecBase_A operator-(const VecBase_A &other)
	{
		VecBase_A temp(*this);
		temp -= other;

		return temp;
	}
	virtual VecBase_A operator*(const float other)
	{
		VecBase_A temp(*this);
		temp *= other;

		return temp;
	}
	virtual VecBase_A operator/(const float other)
	{
		VecBase_A temp(*this);
		temp /= other;

		return temp;
	}

	virtual float operator*(const VecBase_A &other)
	{
		int dotProduct = 0;
		for(int i=0; i<size; i++)
		{
			dotProduct += this->_data[i] * other._data[i];
		}

		return dotProduct;
	}

protected:
	VecBase_A()
	{
	};

	T _data[size];
};


/** Math vector class.
 * The basic vector class.
 * @tparam T The type of the vector coordinates (float, int, ...).
 */
template<typename T, int size>
class Vec : public VecBase_A<T, size>
{
};


/** Specialized vector class for vectors of size 2.
 * A specialized vector class to provide access to the x- and y-coordinate of the
 * vector.
 * @tparam T The type of the vector coordinates (float, int, ...).
 */
template<typename T>
class Vec<T, 2> : public VecBase_A<T, 2>
{
public:
	T &x, &y;
	/** Constructor from 3d-vector.
	 *	Creates a new 2d-vector from a 3d-vector by using homogeneous coordinate transformation.
	 *  @param[in] other The 3d-vector to construct the 2d-vector of.
	 */
	Vec(const VecBase_A<T, 3> &other) : Vec<T, 3>(other._data[0] / other._data[2], other._data[1] / other._data[2]){}

	/** Copy-constructor.
     * Creates a new 2d-vector as a copy of another 2d-vector.
	 * @param[in] other The vector to copy.
	 */
	Vec(const VecBase_A<T, 2> &other) : Vec<T, 2>(other._data[0], other._data[1]){}

	/** Constructor from the two coordinates of the 2d-vector.
	 * Creates a new 2d-vector from its two given coordinates.
	 * @param[in] x		The value to assign as the x-coordinate.
	 * @param[in] y		The value to assign as the y-coordinate.
   	 */
	Vec(T x, T y) : x(this->_data[0]), y(this->_data[1])
	{
		this->_data[0] = x;
		this->_data[1] = y;
	}
};


/** Specialized vector class for vectors of size 3.
 * A specialized vector class to provide access to the x-,y- and z-coordinate of the
 * vector.
 * @tparam T The type of the vector coordinates (float, int, ...).
 */
template<typename T>
class Vec<T, 3> : public VecBase_A<T, 3>
{
public:
	T &x, &y, &z;
	/** Constructor from 4d-vector.
	 *	Creates a new 3d-vector from a 4d-vector by using homogeneous coordinate transformation.
	 *  @param[in] other The 4d-vector to construct the 3d-vector of.
	 */
	Vec(const VecBase_A<T, 4> &other) : Vec<T, 3>(other._data[0] / other._data[3], other._data[1] / other._data[3], other.data[2] / other._data[3]){}
	/** Copy-constructor.
	 * Creates a new 3d-vector as a copy of another 3d-vector.
	 * @param[in] other The vector to copy.
	 */
	Vec(const VecBase_A<T, 3> &other) : Vec<T, 3>(other._data[0], other._data[1], other._data[2]){}
	/** Constructor from 2d-vector.
	 * Creates a new 3d-vector from a 2d vector. The z-coordinate of the vector
	 * will be set to one.
	 * @param[in] other The 2d-vector to construct the 3d-vector of.
	 */
	Vec(const VecBase_A<T, 2> &other) : Vec<T, 3>(other, 1){}
	/** Constructor from 2d-vector with direct setting of the z-ccordinate.
	 * Creates a new 3d-vector from a 2d vector. You also have to specify
	 * the missing z-coordinate.
	 * @param[in] other The 2d-vector to construct the 3d-vector of.
	 * @param[in] z 	The value to assign as the z-coordinate.
	 */
	Vec(const VecBase_A<T, 2> &other, T z) : Vec<T, 3>(other.data[0], other.data[1], z){}
	/** Constructor from the three coordinates of the 3d-vector.
	 * Creates a new 3d-vector from its three given coordinates.
	 * @param[in] x		The value to assign as the x-coordinate.
	 * @param[in] y		The value to assign as the y-coordinate.
	 * @param[in] z		The value to assign as the z-coordinate.
	 */
	Vec(const T x, const T y, const T z) : x(this->_data[0]), y(this->_data[1]), z(this->_data[2])
	{
		this->_data[0] = x;
		this->_data[1] = y;
		this->_data[2] = z;
	}
};


/** Specialized vector class for vectors of size 4.
 * A specialized vector class to provide access to the x-, y-, z- and w-coordinate of the
 * vector.
 * @tparam T The type of the vector coordinates (float, int, ...).
 */
template<typename T>
class Vec<T, 4> : public VecBase_A<T, 4>
{
	T &x, &y, &z, &w;
	/** Constructor from 5d-vector.
	 *	Creates a new 4d-vector from a 5d-vector by using homogeneous coordinate transformation.
	 *  @param[in] other The 5d-vector to construct the 4d-vector of.
	 */
	Vec(const VecBase_A<T, 5> &other) : Vec<T, 4>(other._data[0] / other._data[4], other._data[1] / other._data[4], other.data[2] / other._data[4], other.data[3] / other._data[4]){}
	/** Copy-constructor.
	 * Creates a new 4d-vector as a copy of another 4d-vector.
	 * @param[in] other The vector to copy.
	 */
	Vec(const VecBase_A<T, 4> &other) : Vec<T, 4>(other._data[0], other._data[1], other._data[2], other._data[3]){}
	/** Constructor from 3d-vector.
	 * Creates a new 4d-vector from a 3d vector. The w-coordinate of the vector
	 * will be set to one.
	 * @param[in] other The 3d-vector to construct the 4d-vector of.
	 */
	Vec(const VecBase_A<T, 3> &other) : Vec<T, 4>(other, 1){}
	/** Constructor from 3d-vector with direct setting of the w-ccordinate.
	 * Creates a new 4d-vector from a 3d vector. You also have to specify
	 * the missing w-coordinate.
	 * @param[in] other The 2d-vector to construct the 4d-vector of.
	 * @param[in] w 	The value to assign as the w-coordinate.
	 */
	Vec(const VecBase_A<T, 2> &other, T w) : Vec<T, 3>(other.data[0], other.data[1], other._data[2], w){}
	/** Constructor from the three coordinates of the 3d-vector.
	 * Creates a new 3d-vector from its three given coordinates.
	 * @param[in] x	 The value to assign as the x-coordinate.
	 * @param[in] y	 The value to assign as the y-coordinate.
	 * @param[in] z	 The value to assign as the z-coordinate.
	 * @param[in] w	 The value to assign as the z-coordinate.
	 */
	Vec(const T x, const T y, const T z, const T w) : x(this->_data[0]), y(this->_data[1]), z(this->_data[2], w(this->_data[3]))
	{
		this->_data[0] = x;
		this->_data[1] = y;
		this->_data[2] = z;
		this->_data[3] = w;
	}
};

#endif
